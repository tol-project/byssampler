//////////////////////////////////////////////////////////////////////////////
// FILE    : Generic.tol
// PURPOSE : Implementation of basic classes
//   BysSampler::@Target  
//   BysSampler::@Sampler
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Class @Target
//////////////////////////////////////////////////////////////////////////////
{
  ////////////////////////////////////////////////////////////////////////////
  //Data access are virtual member to avoid overhead and excessive copying
  ////////////////////////////////////////////////////////////////////////////
  //Number of variables to be simulated
  Real _.n;

  //Identifier of target for information purposes
  Text _MID = "";

  ////////////////////////////////////////////////////////////////////////////
  Real putMID(Text mid)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text _MID := mid;
    1
  };
  ////////////////////////////////////////////////////////////////////////////
  Text getMID(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    _MID
  };

  //Returns current drawn values
  VMatrix get.x(Real void);
  //Sets current values
  Real set.x(VMatrix x);

  //Target log-density except a constant
  //This method must be redefined by end user classes in order to define
  //the specific distribution to sample
  Real pi_logDens(VMatrix z);

  //Returns true if the point is in the domain of the target density
  Real is_feasible(VMatrix z);

  ///////////////////////////////////////////////////////////////////////////
  //Will be called after each accepted or rejected simulation
  //This method can be redefined by end user classes
  Real doAfterAccept(Real void) 
  ///////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE [@Target doAfterAccept] 1");
    True 
  };

  ///////////////////////////////////////////////////////////////////////////
  //Will be called after each rejected simulation
  //This method can be redefined by end user classes
  Real doAfterReject(Real void) 
  ///////////////////////////////////////////////////////////////////////////
  { 
    True 
  }

};

//////////////////////////////////////////////////////////////////////////////
Class @Target.Null : @Target
//////////////////////////////////////////////////////////////////////////////
{
  VMatrix get.x(Real void) { ? };
  Real set.x(VMatrix x) { ? };
  Real pi_logDens(VMatrix z) { ? };
  Real is_feasible(VMatrix z) { ? };
  Static @Target New(Real void)
  {
    @Target.Null null = [[ Real _.n = ? ]];
    null
  }
};

//////////////////////////////////////////////////////////////////////////////
Class @Sampler 
//////////////////////////////////////////////////////////////////////////////
{
  //Defines the target distribution
  Set _target;
  //Dimension of space 
  Real _.n = ?;
  //Counting events
  Real _.num_draw = 0;
  //Current acceptance ratio
  Real _.acceptRatio = 1;
  //True if current drawn is accepted
  Real _.accept = True;
  //Internal drawer must be redefined by system internal classes to 
  //implement a given sampling algorithm
  VMatrix _Q_draw_and_prob(VMatrix x);
  //Initializes the sampler
  Real initialize(Real void);


  //Returns current drawn values
  VMatrix get.x(Real void) { (_target[1])::get.x(void) };
  //Sets current values
  Real set.x(VMatrix x) { (_target[1])::set.x(x) };

  //Target log-density except a constant
  //This method must be redefined by end user classes in order to define
  //the specific distribution to sample
  Real pi_logDens(VMatrix z) { (_target[1])::pi_logDens(z) };

  //Returns true if the point is in the domain of the target density
  Real is_feasible(VMatrix z) { (_target[1])::is_feasible(z) };

  Static BysInfDiag::@BysInf.Report.Config reportCfg = 
    BysInfDiag::Inference::GetDefaultReportConfig(?);

  ///////////////////////////////////////////////////////////////////////////
  Real init.Sampler(Real void)
  //This method can be redefined by system internal classes
  ///////////////////////////////////////////////////////////////////////////
  {
    If(!IsUnknown(_.n), False,
    {
    //WriteLn("TRACE @Sampler::init.Sampler");
      Real _.n := (_target[1])::_.n;
      True
    })
  };


  ///////////////////////////////////////////////////////////////////////////
  VMatrix draw(Real void)
  //This method can be redefined by system internal classes
  ///////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE @Sampler::draw 1");
    VMatrix x = get.x(void);
  //WriteLn("TRACE @Sampler::draw 2");
    VMatrix y = _Q_draw_and_prob(x);
  //WriteLn("TRACE @Sampler::draw 3");
    Real set.x(y);
  //WriteLn("TRACE @Sampler::draw 4");
    Real _.num_draw := _.num_draw+1;
  //WriteLn("TRACE @Sampler::draw 5");
    Real doAfterDraw(0);
  //WriteLn("TRACE @Sampler::draw 6");
    y
  };

  ///////////////////////////////////////////////////////////////////////////
  //Will be called after each accepted or rejected simulation
  //This method can be redefined by system internal classes
  Real doAfterDraw(Real void) 
  ///////////////////////////////////////////////////////////////////////////
  { 
    True 
  };

  ///////////////////////////////////////////////////////////////////////////
  Real ShowTrace(Real void) 
  ///////////////////////////////////////////////////////////////////////////
  {
    WriteLn("["+ClassOf(_this)+" "+(_target[1])::getMID(?)+"] Simulation "<<numSim
            +" _.acceptRatio="<<_.acceptRatio);
    True
  };

  ///////////////////////////////////////////////////////////////////////////
  NameBlock mcmc(Real numSamples, Real cacheLength) 
  ///////////////////////////////////////////////////////////////////////////
  { 
    Real numErr.0 = Copy(NError);
    Real time = 0;
    Matrix cache = Constant(0, _.n, 0);
    Matrix cache.log_pi = Constant(0, 1, 0);
    Set set.mcmc = Copy(Empty);
    Set set.mcmc.log_pi = Copy(Empty);
    Real numSim = 0;
    Real append_draw(VMatrix draw)
    {
      Matrix cache := cache << VMat2Mat(draw,true);
    //WriteLn("draw = \n"<<VMat2Mat(draw,true));
      Real log_pi = pi_logDens(draw);
      Matrix cache.log_pi := cache.log_pi << Col(log_pi);
      If(cacheLength & !(numSim%cacheLength), 
      {
        Real ShowTrace(?);
        Set Append(set.mcmc,[[Copy(cache)]]);
        Set Append(set.mcmc.log_pi,[[Copy(cache.log_pi)]]);
        Matrix cache := Rand(0,_.n,0,0);
        Matrix cache.log_pi := Rand(0,1,0,0);
        True 
      });
      Real numSim := numSim+1
    };

    Real numSim := 1;
    Real mtm.t0 = Copy(Time);
    //Real TolOprProfiler.Enabled := True;
    While(And(numSim<=numSamples,NError==numErr.0), append_draw(draw(0)));
    //Real TolOprProfiler.Dump("sampler.csv");

    Real total.time = (Copy(Time) - mtm.t0);
    Real simu.time = total.time/numSim;
    WriteLn("["+ClassOf(_this)+"] Total time elpased: "<<total.time);
    WriteLn("["+ClassOf(_this)+"] Time by simulation: "<<simu.time);

    Matrix mcmc.var = Group("ConcatRows",set.mcmc);
    Matrix mcmc.log_likelihood = Group("ConcatRows",set.mcmc.log_pi);
    Real mcmc.max_log_likelihood = MatMax(mcmc.log_likelihood);
    Matrix mcmc.likelihood = 
      Exp((mcmc.log_likelihood - mcmc.max_log_likelihood)*(1/Columns(mcmc.var)));
    [[mcmc.var, mcmc.log_likelihood, mcmc.likelihood, mcmc.max_log_likelihood]]
  };

  ///////////////////////////////////////////////////////////////////////////
  Real mcmc.save(Real numSamples, Real cacheLength, Text root, Real resume) 
  ///////////////////////////////////////////////////////////////////////////
  { 
    Real numErr.0 = Copy(NError);
    Real time = 0;
    Matrix cache.param = Constant(0, _.n, 0);
    Matrix cache.logpi = Constant(0, 1, 0);
    Text path.mcmc.param = root+"mcmc.param.bbm";
    Text path.mcmc.logpi = root+"mcmc.logpi.bbm";
    Real NS = 0;
    Real numSim = Case(
    !resume,
    {
      Real OSFilRemove(path.mcmc.param); 
      Real OSFilRemove(path.mcmc.logpi); 
      1
    },
    OSFilExist(path.mcmc.param),
    {
      Set mcmc.dim = MatReadDimensions(path.mcmc.param);
      WriteLn("["+ClassOf(_this)+"] Resuming "<<mcmc.dim::Rows+" saved samples.");
      VMatrix last = Mat2VMat(MatReadRows(path.mcmc.param,mcmc.dim::Rows-1,1,1),True); 
      Real set.x(last);
      Real pi_logDens(last);
      mcmc.dim::Rows+1
    },
    1==1,
    {
      1
    });
    Real append_draw(VMatrix draw)
    {
    //WriteLn("draw = \n"<<VMat2Mat(draw,true));
      Real NS:=NS+1;
      Matrix cache.param := cache.param << VMat2Mat(draw,true);
      Real logpi = pi_logDens(draw);
      Matrix cache.logpi := cache.logpi << Col(logpi);
      If(cacheLength & !(numSim%cacheLength), 
      {
        Real ShowTrace(?);
        
        Matrix MatAppendFile(path.mcmc.param, cache.param);
        Matrix MatAppendFile(path.mcmc.logpi, cache.logpi);
        Matrix cache.param  := Rand(0,_.n,0,0);
        Matrix cache.logpi := Rand(0,1,0,0);
        True 
      });
      Real numSim := numSim+1
    };

    Real mtm.t0 = Copy(Time);
    //Real TolOprProfiler.Enabled := True;
    While(And(numSim<=numSamples,NError==numErr.0), append_draw(draw(0)));
    //Real TolOprProfiler.Dump("sampler.csv");

    Real total.time = (Copy(Time) - mtm.t0);
    Real simu.time = total.time/NS;
    WriteLn("["+ClassOf(_this)+"] Total time elpased: "<<total.time);
    WriteLn("["+ClassOf(_this)+"] Time by simulation: "<<simu.time);
    numSamples
  };

  

  ///////////////////////////////////////////////////////////////////////////
  Static Set mcmc.loadAndReport(Text root, Text output.name, Set input.name, 
                                Real length_, Real burnin, Real thinning) 
  ///////////////////////////////////////////////////////////////////////////
  { 
    Text path.mcmc.param = root+"mcmc.param.bbm";
    Text path.mcmc.logpi = root+"mcmc.logpi.bbm";
    Set mcmc.dim = MatReadDimensions(path.mcmc.param);
    Real max = (mcmc.dim::Rows-burnin)/thinning;
    Real length = Min(max,length_);
    Matrix mcmc.param = MatReadRows(path.mcmc.param, burnin, length, thinning);
    BysInfDiag::Inference::report
    (
      output.name, input.name,
      BysSampler::@Sampler::reportCfg,
      mcmc.param
    ) << 
    [[ 
      Matrix mcmc.param;
      Matrix mcmc.logpi = MatReadRows(path.mcmc.logpi, burnin, length, thinning);
      Real max.logpi = MatMax(mcmc.logpi);
      Matrix mcmc.likelihood = Exp(mcmc.logpi - max.logpi) 
    ]]
  };

  ///////////////////////////////////////////////////////////////////////////
  NameBlock mcmc.thin(Real numSamples, Real cacheLength, Real thinning) 
  ///////////////////////////////////////////////////////////////////////////
  { 
    Real numErr.0 = Copy(NError);
    Real time = 0;
    Matrix cache = Constant(0, _.n, 0);
    Matrix cache.log_pi = Constant(0, 1, 0);
    Set set.mcmc = Copy(Empty);
    Set set.mcmc.log_pi = Copy(Empty);
    Real numSim = 0;
    Real append_draw(VMatrix draw)
    {
    //WriteLn("draw = \n"<<VMat2Mat(draw,true));
      Matrix If(!(numSim%thinning),
      {
        cache := cache << VMat2Mat(draw,true);
        Real log_pi = pi_logDens(draw);
        cache.log_pi := cache.log_pi << Col(log_pi)
      });
      If(cacheLength & !(numSim%cacheLength), 
      {
        Real ShowTrace(?);
        Set Append(set.mcmc,[[Copy(cache)]]);
        Set Append(set.mcmc.log_pi,[[Copy(cache.log_pi)]]);
        Matrix cache := Rand(0,_.n,0,0);
        Matrix cache.log_pi := Rand(0,1,0,0);
        True 
      });
      Real numSim := numSim+1
    };

    Real numSim := 1;
    Real mtm.t0 = Copy(Time);
    //Real TolOprProfiler.Enabled := True;
    While(And(numSim<=numSamples,NError==numErr.0), append_draw(draw(0)));
    //Real TolOprProfiler.Dump("sampler.csv");

    Real total.time = (Copy(Time) - mtm.t0);
    Real simu.time = total.time/numSim;
    WriteLn("["+ClassOf(_this)+"] Total time elpased: "<<total.time);
    WriteLn("["+ClassOf(_this)+"] Time by simulation: "<<simu.time);

    Matrix mcmc.var = Group("ConcatRows",set.mcmc);
    Matrix mcmc.log_likelihood = Group("ConcatRows",set.mcmc.log_pi);
    Real mcmc.max_log_likelihood = MatMax(mcmc.log_likelihood);
    Matrix mcmc.likelihood = 
      Exp((mcmc.log_likelihood - mcmc.max_log_likelihood)*(1/Columns(mcmc.var)));
    [[mcmc.var, mcmc.log_likelihood, mcmc.likelihood, mcmc.max_log_likelihood]]
  };
  ////////////////////////////////////////////////////////////////////////////
  Real clear(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Set _target := Copy(Empty);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real __destroy(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    clear(void)
  }
  
};


//////////////////////////////////////////////////////////////////////////////
NameBlock AutoBurnMaxLogPostDens(Matrix LD_, Real q)
//////////////////////////////////////////////////////////////////////////////
{[[
  Matrix LD = LD_;
  Real N = Rows(LD);
  Matrix LDQ = Constant(N,1,MatQuantile(Sub(LD,3*N/4,1,N/4,1),q));
  Matrix LM = Tra(BysSampler::CppTools::LastMax(Tra( LD)));
  Matrix Lm =-Reverse(Tra(BysSampler::CppTools::LastMax(Tra(-Reverse(LD)))));
  Matrix LDMm = LD | LM | Lm;
  Matrix M = { 
    P=Sort(LDMm,[[2]],True); 
    P|SubRow(LM,MatSet(Tra(P))[1]) 
     |SubRow(Lm,MatSet(Tra(P))[1])  
  };
  Real burnin = { Real b=MatSum(LT(LM,LDQ)), If(IsUnknown(b),0,b) };
  Real burntSize = N-burnin;
  Matrix LD.burned = Sub(LD,burnin+1,1,burntSize,1);
  Matrix Burn(Matrix mat_)
  {
    Real r_ = Rows(mat_);
    Real c_ = Columns(mat_);
    If(r_<=burntSize, mat_,
    {
      Real br_ = r_-burntSize;
      Sub(mat_,br_+1,1,burntSize,c_)
    })
  }
]]};


