/* CppTools.cpp: Utilidades para la aceleraci�n de procesos en el paquete 
               BysSampler (Bayesian Sampler)

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */


//Starts local namebock scope
#define LOCAL_NAMEBLOCK _local_namebtntLock_

#if defined(_MSC_VER)
#  include <win_tolinc.h>
#endif

#include <tol/tol_LoadDynLib.h>
#include <tol/tol_bcommon.h>
#include <tol/tol_boper.h>
#include <tol/tol_bnameblock.h>
#include <tol/tol_bmatgra.h>
#include <tol/tol_bvmatgra.h>
#include <tol/tol_bprdist.h>
#include <tol/tol_bprdist_internal.h>
#include <tol/tol_gsl.h>
#include <gsl/gsl_sf.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_sys.h>

#define dMat(arg) ((DMat&)Mat(arg))
#define b2dMat(M) ((DMat&)(M))

//Creates local namebtntLock container
static BUserNameBlock* _local_unb_ = NewUserNameBlock();

//Creates the reference to local namebtntLock
static BNameBlock& _local_namebtntLock_ = _local_unb_->Contens();

//Entry point of library returns the NameBlock to LoadDynLib
//This is the only one exported function 
DynAPI void* GetDynLibNameBlockBysSampler()
{
  BUserNameBlock* copy = NewUserNameBlock();
  copy->Contens() = _local_unb_->Contens();
  return(copy);
}

#define ERR(cond,msg,ret) \
if(showErr && cond) { \
  Error(_MID<<msg); \
  return(ret); \
}

//--------------------------------------------------------------------
static BText QuotientOfWeightSumsFromLog_desc = 
"Dado dos vectores columna de pesos logar�tmicos \n"
"  logWeight_A = ( log(W_A[1]), ..., log(W_A[n_A]) ) \n"
"  logWeight_B = ( log(W_B[1]), ..., log(W_B[n_B]) ) \n"
"devuelve el cociente de las sumas de los pesos:\n"
"  Q = Sum(W_A[j]; j=1...n_A) / Sum(W_B[j]; j=1...n_B)\n"
"Cuando el cociente participa en un producto o cociente de otras "
"magnitudes puede ser conveniente mantener la forma logar�tmica "
"para evitar errores num�ricos en el tratamiento de n�meros muy "
"grandes o muy peque�os.\n" 
"Para evitarlo se pasan los argumentos logWeight_A y logWeight_B "
"cuyo valor previo es irrelevante pues no se utiliza como entrada "
"sino como salida. Se les asignar�n los valores:\n"
"  logWeight_A = log(Sum(W_A[j]; j=1...n_A))\n"
"  logWeight_B = log(Sum(W_B[j]; j=1...n_B))\n"
"\n";
static double QuotientOfWeightSumsFromLog(
  const DMat& logWeight_A,
  const DMat& logWeight_B,
  double& log_sum_A,
  double& log_sum_B,
  bool showErr)
//--------------------------------------------------------------------
{
  static BText _MID = "[BysSampler::CppTools::QuotientOfWeightSumsFromLog] ";
  double max_A = BDat::NegInf();
  double max_B = BDat::NegInf();
  int j;
  int n_A = logWeight_A.Rows();
  int n_B = logWeight_B.Rows();
  double sum_A=0, sum_B=0;
  for(j=0; j<n_A; j++)
  {
    double& lwj = logWeight_A(j,0);
    if(lwj>max_A) { max_A = lwj; }
  }
  ERR(max_A == BDat::NegInf(),
  "Debe haber al menos un peso no nulo "
  "en en el numerador",BDat::Nan());
  ERR(max_A == BDat::PosInf(),
  "Los pesos del numerador deben ser finitos",false);
  for(j=0; j<n_B; j++)
  {
    double& lwj = logWeight_B(j,0);
    if(lwj>max_B) { max_B = lwj; }
  }
  ERR(max_B == BDat::NegInf(),
  "Debe haber al menos un peso no nulo "
  "en en el denominador",BDat::Nan());
  ERR(max_B == BDat::PosInf(),
  "Los pesos del denominador deben ser finitos",false);
  for(j=0; j<n_A; j++)
  {
    double& lwj = logWeight_A(j,0);
    if((lwj!=BDat::NegInf()) && !gsl_isnan(lwj)) 
    {
      sum_A += exp(lwj-max_A);
    }
  }
  for(j=0; j<n_B; j++)
  {
    double& lwj = logWeight_B(j,0);
    if((lwj!=BDat::NegInf()) && !gsl_isnan(lwj)) 
    {
      sum_B += exp(lwj-max_B);
    }
  }
  log_sum_A = log(sum_A)+max_A;
  log_sum_B = log(sum_B)+max_B;
  return(exp(log_sum_A-log_sum_B));
}

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDatQuotientOfWeightSumsFromLog);
DefMethod(1, BDatQuotientOfWeightSumsFromLog, 
  "QuotientOfWeightSumsFromLog", 2, 5, "Matrix Matrix Real Real Real",
  "(Matrix logWeight_A, Matrix logWeight_B"
   "[, Real log_sum_A, Real log_sum_B, Real showErr])",
  QuotientOfWeightSumsFromLog_desc,
  BOperClassify::MatrixAlgebra_);
void BDatQuotientOfWeightSumsFromLog::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[BysSampler::CppTools::QuotientOfWeightSumsFromLog] ";
  DMat& lw_A = (DMat&)Mat(Arg(1));
  DMat& lw_B = (DMat&)Mat(Arg(2));
  double log_sum_A, log_sum_B;
  bool showErr=false;
  if(Arg(5)) { showErr = Real(Arg(5))!=0.0; }
  //int m = (int)Real(Arg(2));
  contens_ = QuotientOfWeightSumsFromLog(
    lw_A, lw_B, log_sum_A, log_sum_B, showErr);
  if(Arg(3)) { Dat(Arg(3)) = log_sum_A; }
  if(Arg(4)) { Dat(Arg(4)) = log_sum_B; }
};

//--------------------------------------------------------------------
static BText RandPropLogWeight_desc = 
"Dado un vector columna de n pesos logar�tmicos \n"
" logWeight = ( log(W[1]), ..., log(W[n])) \n"
"devuelve un vector columna con m muestras aleatorias de la "
"distribuci�n de probabilidad \n"
"\n"
" Pr[X=k] = W[k] / Sum(W[j]; j=1...n) \n"
"\n";
static bool RandPropLogWeight(
   const DMat& logWeight,
         int m,
         DMat& x,
         bool showErr)
//--------------------------------------------------------------------
{
  static BText _MID = "[BysSampler::CppTools::RandPropLogWeight] ";
  double sum = 0, max = BDat::NegInf();
  int i, j, n;
  n = logWeight.Rows();
  DMat weight(n,1), prob(n,1), prob_cum(n+1,1);
//Std(BText("TRACE ")+_MID+" 1 n="+n+" m="+m+"\n");
  for(j=0; j<n; j++)
  {
    double& lwj = logWeight(j,0);
    if(lwj>max) { max = lwj; }
  }
//Std(BText("TRACE ")+_MID+" 2\n");
  ERR(max == BDat::NegInf(),
  "Debe haber al menos un peso no nulo",false);
  ERR(max == BDat::PosInf(),
  "Los pesos deben ser finitos",false);
  x.Alloc(m,1);
//Std(BText("TRACE ")+_MID+" 3\n");
  for(j=0; j<n; j++)
  {
    double& lwj = logWeight(j,0);
    double& wj = weight(j,0);
    if((lwj==BDat::NegInf()) || gsl_isnan(lwj))
    {
      wj = 0.0;
    }
    else
    {
      wj = exp(lwj - max);
    }
    sum += wj; 
  }
//Std(BText("TRACE ")+_MID+" 4\n");
  prob_cum(0,0) = 0;
  for(j=0; j<n; j++)
  {
    prob(j,0) = weight(j,0)/sum; 
    prob_cum(j+1,0) = prob_cum(j,0)+prob(j,0);
  }
  prob_cum(n,0) = 1;
//Std(BText("TRACE ")+_MID+" 5 m="+m+"\n");
  for(i=0; i<m; i++)
  {
    double r = BUniformDist::Random01().Value(); 
    for(j=1; j<=n; j++)
    {
      if(prob_cum(j,0)>=r) { x(i,0) = j; break; }
    }
  }
//Std(BText("TRACE ")+_MID+" 6\n");
  return(true);
}

//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, 
  BMatRandPropLogWeight);
DefMethod(1, BMatRandPropLogWeight, 
  "RandPropLogWeight", 2, 3, "Matrix Real Real",
  "(Matrix logWeight, Real m [, Real showErr])",
  RandPropLogWeight_desc,
  BOperClassify::MatrixAlgebra_);
void BMatRandPropLogWeight::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[BysSampler::CppTools::RandPropLogWeight] ";
  DMat& lw = (DMat&)Mat(Arg(1));
  int m = (int)Real(Arg(2));
  bool showErr=false;
  if(Arg(3)) { showErr = Real(Arg(3))!=0.0; }
  bool ok = RandPropLogWeight(lw, m, (DMat&)contens_, showErr);
  
};


//--------------------------------------------------------------------
DMat ConstrDispUnif(BStandardOperator* g, const DMat& x_)
//--------------------------------------------------------------------
{
  BVMat x;
  BVMat gx;
  DMat gx_;
  x.DMat2dense(x_);
  BSyntaxObject*arg = new BContensVMat(x);
  BList* lst = NCons(arg);
  BSyntaxObject* ret = g->Evaluator(lst);
  if(ret) 
  { 
    gx = VMat(ret);
    DESTROY(ret);
  }
  gx.GetDMat(gx_);
  return(gx_);
}

//--------------------------------------------------------------------
DMat Gaussian01(int n)
//--------------------------------------------------------------------
{
  DMat u(n,1);
  int i;
  for(i=0; i<n; i++)
  {
    u.Data()(i) = BNormalDist::Random01().Value();
  }
  return(u);
}

//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, 
  BMatDispersedRandomWalkHitAndRun);
DefMethod(1, BMatDispersedRandomWalkHitAndRun, 
  "DispersedRandomWalk.HitAndRun", 3, 9, 
  "Code VMatrix Real Real Real Real Real Real Real",
  "(Code g, VMatrix x0, Real size [, Real burnin=0, Real thinning=1"
  ", Real factor=2.0, Real maxTry=5, Real dispersionExponent=0.05"
  ", Real traceEach=100])",
  "Starting at a given feasible point, draws a Hit and Run random walk from "
  "a cuasi-uniform distributed random variable under arbitrary constraining "
  "inequations g(x)<=0, where \n"
  " * g is a funtion declared as VMatrix g(VMatrix x) \n"
  " * x0 is the initial feasible point \n"
  " * size is the final length of the MCMC \n"
  " * burnin is the number of skiped initial points \n"
  " * thinning is the length of skiped internal intervals \n",
  BOperClassify::MatrixAlgebra_);
void BMatDispersedRandomWalkHitAndRun::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[BysSampler::CppTools::DispersedRandomWalk.HitAndRun] ";
  BCode& code = UCode(Arg(1))->Contens();
  BStandardOperator* g  = code.Operator();
  if(!g || (g->MaxArg()!=1) || (g->Grammar()!=GraVMatrix()) || 
     (g->GrammarForArg(1)!=GraVMatrix()))
  {
    Error(I2("Wrong argument function in ConstrainedDispersedUniform "
             "function calling. It must have this API: \n",
             "Funci�n argumento err�nea en llamada a la funci�n "
             "ConstrainedDispersedUniform. la API deber�a ser:\n")+
          "  VMatrix g(VMatrix x) \n");
  }
  BVMat& x0 = VMat(Arg(2));
  DMat x, y, z, y1, z1, u, gx;
  int numVar = x0.Rows();
  int size = (int)Real(Arg(3));
  int burnin = 0;
  int thinning = 1;
  double factor = 2.0;
  int maxTry = 5;
  int traceEach = 100;
  double dispersionExponent = 0.05;
  if(Arg(4)) { burnin = (int)Real(Arg(4)); }
  if(Arg(5)) { thinning = (int)Real(Arg(5)); }
  if(Arg(6)) { factor = Real(Arg(6)); }
  if(Arg(7)) { maxTry = (int)Real(Arg(7)); }
  if(Arg(8)) { dispersionExponent = Real(Arg(8)); }
  if(Arg(9)) { traceEach = Real(Arg(9)); }
  int i, j, k;
  double ratio = 1;
  double avrLength = 0;
  double Gx;
  x0.GetDMat(x);
  gx = ConstrDispUnif(g,x);
  Gx = gx.Max();
  double t0 = time(0);
  if(Gx>=0)
  {
    Error(_MID+" initial point is not an interior point (Gx="<<Gx<<")");
    return;
  } 
  contens_.Alloc(size,numVar);
  for(i=j=0; j<size; i++)
  {
  //Std(BText("TRACE DispersedRandomWalk  i=")<<i<<"\n");
  //Std(BText("TRACE DispersedRandomWalk  ratio=")<<ratio<<"\n");
  //Std(BText("TRACE DispersedRandomWalk  x:\n")<<x.T().Name()<<"\n");
    u = Gaussian01(numVar);
    y=z=x;
    for(k=0; k<maxTry; k++)
    {
      y1 = y + u*ratio;
      gx = ConstrDispUnif(g,y1);
      if(!gx.Rows()) { return; }
      Gx = gx.Max();
      if(Gx<=0) { ratio *= factor; y = y1; }
      else      { ratio /= factor; }
    //Std(BText("TRACE DispersedRandomWalk   y1:\n")<<y1.T().Name()<<"\n");
    //Std(BText("TRACE DispersedRandomWalk   gx:\n")<<gx.T().Name()<<"\n");
    //Std(BText("TRACE DispersedRandomWalk   Gx=")<<Gx<<"\n");
    //Std(BText("TRACE DispersedRandomWalk   ratio=")<<ratio<<"\n");
    } 
    for(k=0; k<maxTry; k++)
    {
      z1 = z - u*ratio;
      gx = ConstrDispUnif(g,z1);
      if(!gx.Rows()) { return; }
      Gx = gx.Max();
      if(Gx<=0) { ratio *= factor; z = z1; }
      else      { ratio /= factor; }
    //Std(BText("TRACE DispersedRandomWalk   z1:\n")<<z1.T().Name()<<"\n");
    //Std(BText("TRACE DispersedRandomWalk   gx:\n")<<gx.T().Name()<<"\n");
    //Std(BText("TRACE DispersedRandomWalk   Gx=")<<Gx<<"\n");
    //Std(BText("TRACE DispersedRandomWalk   ratio=")<<ratio<<"\n");
    } 
    double length = (y-z).FrobeniusNorm();
    avrLength = (avrLength * i + length)/(i+1.0);
    double r = pow(BUniformDist::Random01().Value(),dispersionExponent);
    double s = BUniformDist::Random01().Value();
    if(s<0.5) { x = y*r+z*(1.0-r); }
    else      { x = z*r+y*(1.0-r); }
    if((i>burnin)&&((i-burnin)%thinning==0))
    {
      for(k=0; k<numVar; k++) { contens_(j,k) = x(k,0); }
      j++; 
      if(j%traceEach==0)
      {
        double percent = round(j*10000.0/size)/100.0;
        double elapsed = time(0) - t0;
        double remaining = round(elapsed*(100.0-percent)/percent);
        Std(_MID+" iteration "<<j<<" of "<<size<<" ["<<percent<<"%] "
            "Time remaining: "<<remaining<<" seconds\n");
        Std(BText("TRACE DispersedRandomWalk   ratio=")<<ratio<<"\n");
        Std(BText("TRACE DispersedRandomWalk   avrLength=")<<avrLength<<"\n");
      }
    }
  //Std(BText("TRACE DispersedRandomWalk  y:\n")<<y.T().Name()<<"\n");
  //Std(BText("TRACE DispersedRandomWalk  z:\n")<<z.T().Name()<<"\n");
  //Std(BText("TRACE DispersedRandomWalk  length=")<<length<<"\n");
  }
};

/*
//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, 
  BMatDispersedRandomDikin);
DefMethod(1, BMatDispersedRandomDikin, 
  "DispersedRandomWalk.HitAndRun", 4, 8, 
  "VMatrix VMatrix VMatrix Real Real Real Real",
  "(VMatrix A, VMatrix a, VMatrix x0, Real size ["
  ", Real burnin=0, Real thinning=1"
  ", Real dispersionExponent=0.05"
  ", Real traceEach=100])",
  "Starting at a given feasible point, draws a Dikin random walk from "
  "a cuasi-uniform distributed random variable under arbitrary constraining "
  "inequations g(x)<=0, where \n"
  " * g is a funtion declared as VMatrix g(VMatrix x) \n"
  " * x0 is the initial feasible point \n"
  " * size is the final length of the MCMC \n"
  " * burnin is the number of skiped initial points \n"
  " * thinning is the length of skiped internal intervals \n",
  BOperClassify::MatrixAlgebra_);
void BMatDispersedRandomDikin::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[BysSampler::CppTools::DispersedRandomWalk.Dikin] ";
  BVMat& A = VMat(Arg(1));
  BVMat& a = VMat(Arg(2));
  BVMat& x0 = VMat(Arg(3));
  DMat x, y, z, y1, z1, u, gx;
  int numVar = x0.Rows();
  int size = (int)Real(Arg(4));
  int burnin = 0;
  int thinning = 1;
  int traceEach = 100;
  double dispersionExponent = 0.05;
  if(Arg(4)) { burnin = (int)Real(Arg(5)); }
  if(Arg(5)) { thinning = (int)Real(Arg(6)); }
  if(Arg(8)) { dispersionExponent = Real(Arg(7)); }
  if(Arg(9)) { traceEach = Real(Arg(8)); }
  
}
*/

//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, BMatLastMax);
DefMethod(1, BMatLastMax, "LastMax", 1, 1, 
  "Matrix",
  "(Matrix A)",
  "Returns the max value of all cells before each one in each row of "
  "given matrix A.",
  BOperClassify::MatrixAlgebra_);
//--------------------------------------------------------------------
void BMatLastMax::CalcContens()
//--------------------------------------------------------------------
{
  BMat& A = Mat(Arg(1));
  int k;
  int r = A.Rows();
  int c = A.Columns();
  int s = r*c;
  contens_.Alloc(r,c);
  const BDat* a = A.Data().Buffer();
  BDat* x = contens_.GetData().GetBuffer();
  BDat max = BDat::NegInf();
//Std(BText("[BMatLastMax] ")<<" r="<<r<<" c="<<c<<" s="<<s);
  for(k=0; k<s; k++, x++, a++)
  {
    if(!(k%c)) { max = BDat::NegInf(); }
    if(*a>max) { max = *a; }
  //Std(BText("[BMatLastMax] ")<<" k="<<k<<" a="<<*a<<" max="<<max<<"\n");
    *x = max;
  }
}
