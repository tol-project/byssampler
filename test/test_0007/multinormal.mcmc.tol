#Require BysSampler;

Real numErr.0 := Copy(NError);
Real arima.time = 0;

Real PutRandomSeed(0);


  BysSampler::@MetHas.RandWalk sampler =
//BysSampler::@MetHas.RandRay sampler =
//BysSampler::@MulTryMet.RandRay sampler =
//BysSampler::@MulTryMet.RandWalk sampler =
//BysSampler::@MulTryMet.Inv.RandWalk sampler =
//BysSampler::@GenMulTryMet.RandRay.Exact sampler =
//BysSampler::@GenMulTryMet.RandRay.Interp sampler =
[[
  Set _target = @NameBlock(
//  BysSampler::@MultiNormal.FixedCov::New(nu, cov, x0);
    BysSampler::@MultiNormal.FixedCov.LinTrunc::New(
     modDef::nu, Eye(modDef::n), modDef::x0, modDef::A, modDef::a) );
  Real acceptRatio.target = .25;
  Real tunning.sFactor = 0.99;
  Real _.stepSize = .0001
]];

Real sampler::initialize(?);



  Real If(ObjectExist("Real","sampler::_.k"),
  Real sampler::_.k := 4);

//Real sampler::tunning.Step := 1;
//Real sampler::tunning.Mode := BysSampler::@RandWalk::TunningMode::None;

Matrix cache = Constant(0, sampler::_.n, 0);
Matrix cache.log_pi = Constant(0, 1, 0);
Set set.mcmc = Copy(Empty);
Set set.mcmc.log_pi = Copy(Empty);

Real numSim = 0;

Real append_draw(VMatrix draw)
{
  Matrix cache := cache << VMat2Mat(draw,true);
  Matrix cache.log_pi := cache.log_pi << Col(sampler::_.pi_logDens_x);
  If(!(numSim%100), 
  {
    WriteLn("Simulation "<<numSim
    +" _.acceptRatio="<<sampler::_.acceptRatio
    +" _.stepSize="<<sampler::_.stepSize
//  +" _.u="<<Matrix VMat2Mat(sampler::_.u, True)
//  +" _.L="<<Matrix SubDiag(VMat2Mat(sampler::_.L))
    );
    Set Append(set.mcmc,[[Copy(cache)]]);
    Set Append(set.mcmc.log_pi,[[Copy(cache.log_pi)]]);
    Matrix cache := Rand(0,sampler::_.n,0,0);
    Matrix cache.log_pi := Rand(0,1,0,0);
    True 
  });
  Real numSim := numSim+1
};

Real burnin = 0;
Real sampleLength = burnin+2000;
Real numSim := 1;
Real mtm.t0 = Copy(Time);
//Real TolOprProfiler.Enabled := True;
While(And(numSim<=sampleLength,NError==numErr.0), append_draw(sampler::draw(0)));
//Real TolOprProfiler.Dump("sampler.csv");

Real mtm.time = (Copy(Time) - mtm.t0)/numSim;
Real mtm.speed = sampler::_.acceptRatio / mtm.time;
WriteLn("Time by MTM simulation "<<mtm.time);

Matrix mcmc = Group("ConcatRows",set.mcmc);
Matrix mcmc.log_pi = Group("ConcatRows",set.mcmc.log_pi);
Real max_log_pi = MatMax(mcmc.log_pi);
Matrix mcmc.pi = Exp(mcmc.log_pi-max_log_pi);

Real numErr = Copy(NError) - numErr.0;
Real numWar = Copy(NWarning) - numWar.0;

Text report.path = "mtrymh.tab";

Real raftery.burnin = ?;
Real raftery.thinning = ?;
Real raftery.rate = ?;
Real raftery.speed = ?;


Matrix cmp = mcmc | Group("ConcatColumns", For(1,VRows(modDef::nu),Matrix(Real j) 
{
  Constant(Rows(mcmc),1,VMatDat(modDef::nu,j,1))
}));
Set raftery.report = If(numErr, Copy(Empty),
{[[
  Matrix raftery=BysInfDiag::RCODA::raftery.diag(mcmc,{ [[
    Real verbose=TRUE, 
    Real varByCol=TRUE,
    Real q = 0.025;
    Real r = 0.007;  
    Real s = 0.950;  
    Real eps = 0.001
  ]]});
  Real raftery.burnin := MatMax(SubCol(raftery,[[1]]));
//Real burnin := Min(raftery.burnin,500);
  Real raftery.thinning := MatMax(SubCol(raftery,[[4]]));
  Real raftery.rate := (numSim-raftery.burnin)/(numSim*raftery.thinning);
  Real raftery.speed := raftery.rate / mtm.time
]]});
  
Matrix mcmc.sub = SubRow(mcmc, Range(burnin+1,Rows(mcmc),1));
Matrix x.avr = Tra(Constant(1,Rows(mcmc.sub),1/Rows(mcmc.sub))*mcmc.sub);
Matrix x.avr_N = Group("ConcatRows",NCopy(Rows(mcmc.sub), Matrix Tra(x.avr)));
Matrix x.cov = MtMSqr(mcmc.sub-x.avr_N)*1/Rows(mcmc.sub);
VMatrix x_.avr = Mat2VMat(x.avr);
Real x.avr.pi_logDens = sampler::pi_logDens(x_.avr);
Real nu.avr.pi_logDens = sampler::pi_logDens(modDef::nu);
VMatrix nu.cmp = modDef::nu | x_.avr;

/* */
