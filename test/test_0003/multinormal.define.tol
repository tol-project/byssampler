#Require BysSampler;
Real numErr.0 = Copy(NError);
Real numWar.0 = Copy(NWarning);

Real rndSeed = GetRandomSeed(0);
WriteLn("Current Random Seed = "<<rndSeed);

WriteLn("Building TRUE model ...");

NameBlock modDef = [[
Real n = IntRand(34,34);
Real k = Round(n*.95-2);
Real m = Round(n*Rand(1,5));
Real r = Round(n*Rand(0.05,0.95));
VMatrix prior.sign = GE(Rand(n,1,0,1),0.7) - GE(Rand(n,1,0,1),0.7);
Real known.sign.ratio = VMatAvr(Abs(prior.sign)); 
Real lb = -5;
Real ub = +5;
VMatrix LB = IfVMat(GT(prior.sign,0),0,lb);
VMatrix UB = IfVMat(LT(prior.sign,0),0,ub);

VMatrix nu  = {
  VMatrix aux = Rand(n,1,0.1,.9);
  LB $* aux + UB $* (-aux+1)
};

VMatrix prior.X0 = Round(Rand(n,n,-5,5,"Cholmod.R.Sparse",1+10*n))/5;
VMatrix prior.N0 = GT(Abs(prior.X0)*Constant(n,1,1),0);
Real prior.ratio = VMatAvr(Abs(prior.N0)); 
VMatrix prior.X = prior.X0 + Eye(n,n,0,1);

VMatrix prior.lambda = IfVMat(prior.N0,Rand(n,1,.1,1),Rand(n,1,100,100));
VMatrix prior.E = IfVMat(prior.N0,Gaussian(n,1,0,1) $* prior.lambda,0);

VMatrix prior.lambda0 = IfVMat(prior.N0,prior.lambda $* Rand(n,1,0.5,2),
                               prior.lambda);

VMatrix prior.Y = IfVMat(prior.N0,prior.X*nu+prior.E,0);

VMatrix X = {
  VMatrix X0 = Rand(m,k,-1,1,"Cholmod.R.Sparse",.2*k*m)/Sqrt(k);
  VMatrix LC = Rand(k,n-k,-1,1,"Cholmod.R.Sparse",.3*k*(n-k))/Sqrt(k);
  VMatrix X1 = X0*LC + Gaussian(m,n-k,0,1E-7);
  X0 | X1
};

//Set X.svd = SVD(VMat2Mat(X));

VMatrix Y_ = X*nu;  
Real sigma = Rand(0.05,0.10)*(VMatMax(Y_)-VMatMin(Y_));
VMatrix E = Gaussian(m,1,0,sigma);
VMatrix Y = X*nu+E;
VMatrix A.1 = Rand(r,n,-1,1,"Cholmod.R.Sparse",3*n+r);
VMatrix A.2 = -Eye(n);
VMatrix A.3 =  Eye(n);
VMatrix A = A.1<<A.2<<A.3;
VMatrix b0_ = nu+Rand(n,1,lb*.1,ub*.1);
VMatrix a.1 = A.1*b0_+Rand(r,1,0,1); 
//VMatrix a.1 = (A.1*nu+Rand(r,1,-0.1,1)); 
VMatrix a.2 = -LB;
VMatrix a.3 = UB;
VMatrix a = a.1<<a.2<<a.3;

Real nu_is_feasible = {
  VMatrix g = A*nu-a;
  Real G = VMatMax(g);
  Real fs = G<=0;
  fs
};

VMatrix b0 =
{
  Real numSample = 10 + Round(Sqrt(n));
  VMatrix mcmc = BysPrior::@MultNormal.Trunc::
    DrawInPolytopeFromPoint(A, a, b0_, numSample, 1E-5);
  SubCol(mcmc,[[numSample]])
};

Real b0_is_feasible = {
  VMatrix g = A*b0-a;
  Real G = VMatMax(g);
  Real fs = G<=0;
  fs
};


Set QualityCheck(Text id, Matrix mcmc, Real burnin, Real time.mcmc)
{[[
  Matrix mcmc.sub = SubRow(mcmc, Range(burnin+1,Rows(mcmc),1));
  Set sample = For(1,Columns(mcmc.sub),Matrix(Real j) { SubCol(mcmc.sub,[[j]])});
  Matrix x.avr = Tra(Constant(1,Rows(mcmc.sub),1/Rows(mcmc.sub))*mcmc.sub);
  Matrix x.avr_N = Group("ConcatRows",NCopy(Rows(mcmc.sub), Matrix Tra(x.avr)));
  Matrix x.cov = MtMSqr(mcmc.sub-x.avr_N)*1/Rows(mcmc.sub);
  VMatrix x_.avr = Mat2VMat(x.avr);
  //Real x.avr.pi_logDens = sampler::pi_logDens(x_.avr);
  //Real nu.avr.pi_logDens = sampler::pi_logDens(_this::nu);
  Matrix beta.cmp = VMat2Mat(modDef::nu | x_.avr);
  Real beta.dist = Sqrt(VMatAvr((modDef::nu - x_.avr)^2));
  VMatrix residuals = modDef::Y-modDef::X*x_.avr;
  Matrix res.cmp = VMat2Mat(modDef::E | residuals);
  Real res.dist = Sqrt(VMatAvr((modDef::E - residuals)^2));
  
  Set info = {
    Set info = [[
      Real rndSeed;
      Real time = Copy(time.mcmc);  
      Real n = Copy(_this::n);
      Real m = Copy(_this::m);
      Real r = Copy(_this::r);
      Real sigma = Copy(_this::sigma);
      Real se.true = Copy(Sqrt(VMatAvr(_this::E^2)));
      Real se.mcmc = Copy(Sqrt(VMatAvr(residuals^2)));
      Real G.mcmc = Copy(VMatMax(_this::A*x_.avr-_this::a));
      Real G.true = Copy(VMatMax(_this::A*_this::nu-_this::a));
      Real beta.dist;
      Real res.dist
    ]];
    Set View(info,"Std");
    WriteLn("");
    info
  }
]]}



]];

WriteLn("  Number of parameters: "<<modDef::n);
WriteLn("  Number of data: "<<modDef::m);
WriteLn("  Number of constraints: "<<modDef::r);

/* */
