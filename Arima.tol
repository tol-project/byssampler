//////////////////////////////////////////////////////////////////////////////
// FILE    : Arima.tol
// PURPOSE : Implementation of Class BysSampler::@Arima
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Class @Arima : @Target
//Base class for ARIMA models.
//Initial values of ARIMA difference equation are treated as a complementary 
//Gibbs block that will be simulated after each simulation or ARMA parameters
//////////////////////////////////////////////////////////////////////////////
{
  //ARIMA noise
  VMatrix _.z;
  //ARMA parameters
  VMatrix _.x = Rand(0,0,0,0);
  //Nuber of ARMA parameters
  Real _.n = ?;
  //Standard deviation 
  Real _.sigma;
  //Set of @ARIMAStruct elements
  Set _.arima = Copy(Empty);
  //Aggregated autoregressive polynomial
  Polyn  _.ar = 1;
  //Aggregated moving-average polynomial
  Polyn  _.ma = 1;
  //Internal representation of ARMA parameters used in conversions
  Set _.arma = Copy(Empty);
  //Internal ARIMA evaluation handler
  NameBlock _.almagro = [[ Real void=? ]];
  //Initial values of ARIMA difference equation
  VMatrix _.u0 = Rand(0,0,0,0);
  VMatrix _.z0 = Rand(0,0,0,0);
  VMatrix _.a0 = Rand(0,0,0,0);
  
  ////////////////////////////////////////////////////////////////////////////
  //Creates a new instance
  Static @Arima New(
     VMatrix z, 
     Real sigma, 
     Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    @Arima new = 
    [[
      VMatrix _.z = z;
      Real _.sigma = sigma
    ]];
    Real new::init.Arima(arima);
    new
  };

  ////////////////////////////////////////////////////////////////////////////
  //Converts a Set of @ARIMAStruct elements in a column vector
  VMatrix arima2col(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
  //Mat2VMat(SetCol(ARIMAToParam(arima)))
    Set arma = EvalSet(arima, Set(@ARIMAStruct f)
    {
      SetOfPolyn(1-f->AR, 1-f->MA)
    });
    Mat2VMat(Tra(GetNumeric(arma)))
  };
  
  ////////////////////////////////////////////////////////////////////////////
  //Converts a column vector in a Set of @ARIMAStruct elements with the same
  //structure than initial definition of member _.arima
  Set col2arima(VMatrix x)
  ////////////////////////////////////////////////////////////////////////////
  {
//  ParamToARIMA(_.arima, MatSet(Tra(VMat2Mat(x)))[1])
    Set arma = DeepCopy(_.arma, VMat2Mat(x));
    Set arima =  DeepCopy(_.arima); 
    Set For(1,Card(_.arima), Real(Real k)
    {
      Polyn arima[k]->AR := 1-arma[k][1];
      Polyn arima[k]->MA := 1-arma[k][2];
      True
    });
    arima
  };

  ////////////////////////////////////////////////////////////////////////////
  //Draws Gibbs block of ARMA initial values
  Real drawArmaInitValues(Real recalcHandler)
  ////////////////////////////////////////////////////////////////////////////
  {
    //Evaluation handler must be recalculated just when parameters has changed
    Real If(recalcHandler,
    {
      Polyn  _.ar := ARIMAGetAR(_.arima);
      Polyn  _.ma := ARIMAGetMA(_.arima);
      NameBlock _.almagro := ARMAProcess::Eval.Almagro(_.ar,_.ma,_.z,_.sigma);
      True
    });
    VMatrix _.u0 := _.almagro::Draw.U_cond_Z(0);
    VMatrix _.z0 := _.almagro::Get.Z0(_.u0);
    VMatrix _.a0 := _.almagro::Get.A0(_.u0);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  //Initialize all members to be congruent with a given ARIMA model
  Real init.Arima(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set _.arima := DeepCopy(arima);
    Set _.arma := EvalSet(_.arima, Set(@ARIMAStruct f)
    {
      SetOfPolyn(1-f->AR, 1-f->MA)
    });
    VMatrix _.x := arima2col(_.arima);
    Real ok = drawArmaInitValues(True);
    Real _.n := VRows(_.x);
    //This is the theorical acceptance ratio for single-try
    ok
  };
  
  Real penalty_coef = 1E16;
  
  ////////////////////////////////////////////////////////////////////////////
  //Internal representation of target log-density of an ARIMA model
  Real pi_logDens_(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    Polyn ar = ARIMAGetAR(arima);
    Polyn ma = ARIMAGetMA(arima);
    Real G3 = SetSum(EvalSet(arima,Real(@ARIMAStruct factor)
    {
      Real gAr = 1-StationaryValue(factor->AR);
      Real gMa = 1-StationaryValue(factor->MA);
      Real gAr3p = If(gAr<=0,0,gAr^3);
      Real gMa3p = If(gMa<=0,0,gMa^3);
      gAr3p+gMa3p
    }));
    Real log_pi = _.almagro::LogLH.Z_cond_U(ar,ma,_.z0,_.a0); 
    log_pi - penalty_coef*G3
//  log_pi
  };
  
  ////////////////////////////////////////////////////////////////////////////
  //MEMEBERS AND METHODS INHERITED FROM BysSampler::@Target
  ////////////////////////////////////////////////////////////////////////////

  //Returns current drawn values
  VMatrix get.x(Real void) { Copy(_.x) };
  //Sets current drawn values
  Real set.x(VMatrix x) 
  { 
    VMatrix _.x := Copy(x); 
    Set _.arima := col2arima(_.x);
    True 
  };

  ////////////////////////////////////////////////////////////////////////////
  //Returns true if the point is in the domain of the target density
  Real is_feasible(VMatrix z)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set arima = col2arima(z);
    Polyn ar = ARIMAGetAR(arima);
    Polyn ma = ARIMAGetMA(arima);
    And(IsStationary(ar), IsStationary(ma))
  };

  ////////////////////////////////////////////////////////////////////////////
  //target log-density (black-box)
  Real pi_logDens(VMatrix z)
  ////////////////////////////////////////////////////////////////////////////
  {
    pi_logDens_(col2arima(z))
  };

  ////////////////////////////////////////////////////////////////////////////
  Real doAfterAccept(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@Arima doAfterAccept] 1 ");
    Real drawArmaInitValues(void)
  }


};

